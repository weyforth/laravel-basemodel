<?php

namespace Weyforth\BaseModel;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Validator;

class BaseModel extends Eloquent{
	protected $errors = array();
	protected $callable = array();
	protected $dynamic = array();
	// public static $boolean;
	protected $rules = array(
	);
	public $ruleSet = NULL;
	protected $sometimes = array(
	);

	static public function boot(){
		parent::boot();

		if(!isset(static::$manualValidation)){
			static::saving(function($post){
				$result = $post->validate();
				if(!$result) return false;
			});
		}
	}

	public function validate(){
		$that = $this;
		$rules = $this->ruleSet == NULL ? $this->rules : $this->rules[$this->ruleSet];

		$validation = Validator::make($this->attributes, $this->getValidationRules($rules));
		$sometimes = $this->sometimes;
		foreach ($sometimes as $sometime) {
			$function = $sometime[2];
			$validation->sometimes($sometime[0], $this->filterRule($sometime[1]), function($val) use($that, $function){
				return $that->$function($val);
			});
		}
		if($validation->passes()) return true;
		$this->errors = $validation->messages();

		return false;
	}

	public function replaceRuleAttribute($matches){
		$key = $matches[1];
		return $this->$key ?: NULL;
	}

	protected function filterRule($value){
		return preg_replace_callback("/\{([^}]*)\}/", array($this, 'replaceRuleAttribute'), $value);
	}

	public function getValidationRules($rules){
		foreach ($rules as $key => $value) {
			$rules[$key] = $this->filterRule($value);
		}
		return $rules;
	}

	public function errors(){
		return $this->errors;
	}

	public function setAttributes($array){
		foreach ($array as $key => $value) {
			if(strpos($key, '_') !== 0)
				$this->setAttribute($key, $value);
		}
	}


	public function validateDomainName($attribute, $value, $parameters){
		$domain = preg_match('/^[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$/', $value);
		$www = Str::startsWith($value, 'www.');

		return $domain && !$www;
	}

	/**
	 * Compares a date field against the current date plus a number of minutes, returns true if the date
	 * is in the future, false otherwise.
	 *
	 * @usage future_date_minutes:30
	 * @return boolean
	 */
	public function validateFutureDateMinutes($attribute, $value, $parameters){
		return strtotime($value) > time() + ($parameters[0] * 60);
	}

	/**
	 * Helper function for date field validation. Replaces :minutes
	 * placeholder with number of minutes specified
	 *
	 * @return string
	 */
	public function replaceFutureDateMinutes($message, $attribute, $rule, $parameters){
		return str_replace(array(':minutes'), $parameters[0], $message);
	}

	/**
	 * Compares a date field against the current date, returns true if the date
	 * is in the future, false otherwise.
	 *
	 * @usage future_date
	 * @return boolean
	 */
	public function validateFutureDate($attribute, $value, $parameters){
		return strtotime($value) > time();
	}

	/**
	 * Compares one date field against another, returns true if the first
	 * is within the second date plus specified number of days, false otherwise.
	 *
	 * @usage max_diff_date:secondField,days
	 * @return boolean
	 */
	public function validateMaxDiffDate($attribute, $value, $parameters){
		return strtotime($value) <= strtotime(Input::get($parameters[0])) + ($parameters[1] * 24 * 60 * 60);
	}

	/**
	 * Compares one date field against another, returns true if the first
	 * is later than the second, false otherwise.
	 *
	 * @usage after_date:secondField
	 * @return boolean
	 */
	public function validateAfterDate($attribute, $value, $parameters){
		return strtotime($value) > strtotime(Input::get($parameters[0]));
	}

	/**
	 * Helper function for date field validation. Replaces :other
	 * placeholder with name of other field
	 *
	 * @return string
	 */
	public function replaceAfterDate($message, $attribute, $rule, $parameters){
		return str_replace(array(':other'), str_replace('_', ' ', $parameters[0]), $message);
	}

	/**
	 * Helper function for timezone validation, returns an array
	 * of all the timezones php supports.
	 *
	 * @return array
	 */
	protected function get_timezone_list(){
		$conts = array(
			'Africa' => DateTimeZone::AFRICA,
			'Amercia' => DateTimeZone::AMERICA,
			'Antarctica' => DateTimeZone::ANTARCTICA,
			'Arctic' => DateTimeZone::ARCTIC,
			'Asia' => DateTimeZone::ASIA,
			'Atlantic' => DateTimeZone::ATLANTIC,
			'Australia' => DateTimeZone::AUSTRALIA,
			'Europe' => DateTimeZone::EUROPE,
			'India' => DateTimeZone::INDIAN,
			'Pacific' => DateTimeZone::PACIFIC
		);

		$return = array();

		foreach ($conts as $cont => $code) {
			$timezoneArray = array();
			$timezones = DateTimeZone::listIdentifiers($code);
			foreach ($timezones as $timezone) {
				$timezoneArray[] = $timezone;
			}
			$return[$cont] = $timezoneArray;
		}

		return $return;
	}

	/**
	 * Validates a timezone string against a list of php supported timezones.
	 *
	 * @usage valid_timezone
	 * @return boolean
	 */
	public function validateValidTimezone($attribute, $value, $parameters){
		$contsTimezones = $this->get_timezone_list();
		foreach ($contsTimezones as $cont => $timezones) {
			if(in_array($value, $timezones)) return true;
		}
		return false;
	}

	public function toCallableObject(){

		$return = $this->toArray();

		foreach ($this->callable as $method) {
			$return[$method] = $this->$method();
		}

		foreach ($this->dynamic as $dynamic) {
			$return[$dynamic] = $this->$dynamic;
		}

		return $return;
	}
}
